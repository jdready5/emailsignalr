﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading;   /// use ASync methods rather than threads in .NET world....VERY IMPORTANT.....
using System.Web.Http;
using EmailSignalRTest.Hubs;
using EmailSignalRTest.Models;
using MySql.Data.MySqlClient;
using System.Data;
using System.Text;

namespace EmailSignalRTest.Controllers
{
    [InvalidModelStateFilter]
    public class EmailController : ApiControllerWithHub<EmailHub>
    {
        //test connection to db....
        public MySqlConnection connection = null;
        string connectionString = "Server=localhost;Database=mytest;Uid=root;Pwd=corNField47167";

        // temp static "db" for testing only, will hook up to our database in next step.....
        private static List<Email> db = new List<Email>
        {
            new Email { ID = 0, Title = "From: jeff@gmail.com, Subj: NMP Down" },
            new Email { ID = 1, Title = "From: jeff.ready@hotmail.com, Subj: Website issues" },
            new Email { ID = 2, Title = "From: jeff@blackwellsoftware.com, Subj: Question for you", Finished = true }
        };
        private static int lastId = db.Max(tdi => tdi.ID);

        public IEnumerable<Email> GetEmailItems()
        {
            //test db conn....
         /*   string qry = "select * from email";
            MySqlConnection connection = new MySqlConnection(connectionString);
            MySqlCommand cmd = new MySqlCommand(qry, connection);
            connection.Open();
            MySqlDataReader dr = cmd.ExecuteReader();
            List<Email> db = new List<Email>();
            Email e = new Email();
            DataSet ds = new DataSet();
            DataTable dt = new DataTable();
            ds.Tables.Add(dt);
            ds.EnforceConstraints = false;
            dt.Load(dr);
            int i = 0;
            foreach(DataRow r in ds.Tables[0].Rows)
            {

               // e.ID = r.Field<int>("id");
               // e.Title = r.Field<string>("fromaddress");
                db[i].ID = r.Field<int>("id");
                db[i].Title = r.Field<string>("fromaddress");
                i++;
            }
            
            dr.Close();   */
            
            lock (db)
                return db.ToArray();
        }

        public Email GetToDoItem(int id)
        {
            lock (db)
            {
                var item = db.SingleOrDefault(i => i.ID == id);
                if (item == null)
                    throw new HttpResponseException(
                        Request.CreateResponse(HttpStatusCode.NotFound)
                    );

                return item;
            }
        }

        public HttpResponseMessage PostNewEmailItem(Email item)
        {
            lock (db)
            {
                // Add item to the "database"
                item.ID = 0;// Interlocked.Increment(ref lastId);
                db.Add(item);

                // Notify the connected clients
                Hub.Clients.addItem(item);

                // Return the new item, inside a 201 response
                var response = Request.CreateResponse(HttpStatusCode.Created, item);
                string link = Url.Link("apiRoute", new { controller = "email", id = item.ID });
                response.Headers.Location = new Uri(link);
                return response;
            }
        }

        public Email PutUpdatedEmailItem(int id, Email item)
        {
            lock (db)
            {
                // Find the existing item
                var toUpdate = db.SingleOrDefault(i => i.ID == id);
                if (toUpdate == null)
                    throw new HttpResponseException(
                        Request.CreateResponse(HttpStatusCode.NotFound)
                    );

                // Update the editable fields and save back to the "database"
                toUpdate.Title = item.Title;
                toUpdate.Finished = item.Finished;

                // Notify the connected clients
                Hub.Clients.updateItem(toUpdate);

                // Return the updated item
                return toUpdate;
            }
        }

        public HttpResponseMessage DeleteEmailItem(int id)
        {
            lock (db)
            {
                int removeCount = db.RemoveAll(i => i.ID == id);
                if (removeCount <= 0)
                    return Request.CreateResponse(HttpStatusCode.NotFound);

                // Notify the connected clients
                Hub.Clients.deleteItem(id);

                return Request.CreateResponse(HttpStatusCode.OK);
            }
        }
    }
}
