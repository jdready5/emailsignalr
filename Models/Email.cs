﻿using System.ComponentModel.DataAnnotations;

namespace EmailSignalRTest.Models
{
    public class Email
    {
        public int ID { get; set; }

        [Required]
        public string Title { get; set; }

        public bool Finished { get; set; }

       
    }
}