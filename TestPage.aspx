﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="TestPage.aspx.cs" Inherits="EmailSignalRTest.TestPage" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <script type="text/javascript" src="js/jquery-1.7.2.js"></script>
<script type="text/javascript" src="js/knockout-2.1.0.js"></script>
<script type="text/javascript" src="js/jquery.signalR-0.5.0.js"></script>
<script type="text/javascript" src="signalr/hubs"></script>
<script type="text/javascript" src="js/email.js"></script>
    <form id="form1" runat="server"  data-bind="submit: sendCreate">
        Task:
        <input type="text" name="title" id="title" data-bind="value: addItemTitle, valueUpdate: 'afterkeydown'" />
        <input type="submit" title="Add a new item" value="Add" disabled="disabled" data-bind="enable: addItemTitle().length > 0" />
        </form>
    <section data-bind="visible: items().length > 0" style="display: none">
        <h1>Emails in Progress</h1>
        <table data-bind="foreach: items">
            <tr>
                <td>
                    <input type="checkbox" data-bind="checked: finished" />
                    <span data-bind="text: title, css: { finished: finished }"></span>
                </td>
                <td><input type="button" value="Delete" data-bind="click: remove" /></td>
                <td><input type="text" id="handledby" value="Handled by Jeff Ready" style="display:none" /></td>
            </tr>
        </table>
    </section>
    
</body>
</html>
