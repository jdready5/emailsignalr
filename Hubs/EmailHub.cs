﻿using SignalR.Hubs;

namespace EmailSignalRTest.Hubs
{
    // This hub has no inbound APIs, since all inbound communication is done
    // via the HTTP API. It's here for clients which want to get continuous
    // notification of changes to the database.
    [HubName("email")]
    public class EmailHub : Hub { }

    // following code to impliment later.....
    
    //public void TellAll (string message) {
    //   Caller.doSomething(message);
    //   Clients.doSomething(message);
    //   Clients[id].doSomething(message);
    //}

    // example only...will change later...

    //public string Echo(string value) {
    //  return value;
    //}
}
