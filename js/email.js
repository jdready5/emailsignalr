﻿/// <reference path="jquery-1.7.2.js" />
/// <reference path="knockout-2.1.0.js" />

function EmailViewModel() {
    var self = this;

    function EmailItem(root, id, title, finished) {
        var self = this,
            updating = false;

        self.id = id;
        self.title = ko.observable(title);
        self.finished = ko.observable(finished);

        self.remove = function () {
            root.sendDelete(self);
        };

        self.update = function (title, finished) {
            updating = true;
            self.title(title);
            self.finished(finished);
            updating = false;
        };

        self.finished.subscribe(function () {
            if (!updating) {
                root.sendUpdate(self);
                document.getElementById('handledby').style.display = 'block';
            }
        });
    };

    self.addItemTitle = ko.observable("");
    self.items = ko.observableArray();

    self.add = function (id, title, finished) {
        self.items.push(new EmailItem(self, id, title, finished));
    };

    self.remove = function (id) {
        self.items.remove(function (item) { return item.id === id; });
    };

    self.update = function (id, title, finished) {
        var oldItem = ko.utils.arrayFirst(self.items(), function (i) { return i.id === id; });
        if (oldItem) {
            oldItem.update(title, finished);
        }
    };

    self.sendCreate = function () {
        $.ajax({
            url: "/api/email",
            data: { 'Title': self.addItemTitle(), 'Finished': false },
            type: "POST"
        });

        self.addItemTitle("");
    };

    self.sendDelete = function (item) {
        $.ajax({
            url: "/api/email/" + item.id,
            type: "DELETE"
        });
    }

    self.sendUpdate = function (item) {
        $.ajax({
            url: "/api/email/" + item.id,
            data: { 'Title': item.title(), 'Finished': item.finished() },
            type: "PUT"
        });
    };
};

$(function () {
    var viewModel = new EmailViewModel(),
        hub = $.connection.email;

    ///var result = hub.echo("value");
    ///hub.tellAll("Hello, world!");
    //hub.doSomething = function(message) {
    //   do something with message
    //};

    ko.applyBindings(viewModel);

    hub.addItem = function (item) {
        viewModel.add(item.ID, item.Title, item.Finished);
    };
    hub.deleteItem = function (id) {
        viewModel.remove(id);
    };
    hub.updateItem = function (item) {
        viewModel.update(item.ID, item.Title, item.Finished);
    };

    $.connection.hub.start();

    $.get("/api/email", function (items) {
        $.each(items, function (idx, item) {
            viewModel.add(item.ID, item.Title, item.Finished);
        });
    }, "json");
});
